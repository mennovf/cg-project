from PIL import Image
import sys, os, re, math


def to_le(a):
    GAMMA = 2.2
    return math.pow(a / 255.0, GAMMA)


def rmse(im1, im2):
    l1 = list(im1.getdata())
    l2 = list(im2.getdata())
    return math.sqrt(sum(map(lambda (a, b): math.pow(to_le(a) - to_le(b), 2), zip(l1, l2))) / len(l1))


def mean(im):
    l = list(im.getdata())
    return float(sum(l)) / len(l)

ref = sys.argv[1]
base = sys.argv[2]

FLOAT = r'[+-]?([0-9]*[.])?[0-9]+'
filere = re.compile(base+r'('+FLOAT+r')k'+FLOAT+r'm.png')

files = filter(lambda f: filere.match(f), os.listdir('.'))
files = sorted(files, key=lambda f: float(filere.match(f).group(1)))

REF = Image.open(ref).convert('L')
MREF = mean(REF)
with open(base+'_rmse.txt', 'w') as out:
    for cmp in files:
        n_rays = int(float(filere.match(cmp).group(1)) * 1000)
        CMP = Image.open(cmp).convert('L')
        v = rmse(REF, CMP)
        out.write('%d %f %f\n' % (n_rays, v, MREF))
