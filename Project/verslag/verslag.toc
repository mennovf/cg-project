\select@language {dutch}
\contentsline {section}{\numberline {1}Inleiding}{2}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {2}Path Tracer}{2}
\contentsline {subsection}{\numberline {2.1}Vergelijking Directe Belichting}{2}
\contentsline {subsection}{\numberline {2.2}Grootte van de Lichtbron}{4}
\contentsline {subsection}{\numberline {2.3}Branching Factor}{5}
\contentsline {subsection}{\numberline {2.4}Indirecte Belichting}{6}
\contentsline {section}{\numberline {3}Hybride}{7}
\contentsline {section}{\numberline {4}Caustics}{8}
\contentsline {section}{\numberline {5}Conclusie}{9}
\contentsline {section}{\numberline {6}Progressies}{10}
