\documentclass[]{article}
\usepackage[dutch]{babel}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{float}
\usepackage{cleveref}
\usepackage{subfig}
\usepackage{amsmath}

%opening
\title{Computergrafiek - Project}
\author{Menno Vanfrachem}

\begin{document}

\maketitle

\tableofcontents

\newpage

\section{Inleiding}
Dit verslag gaat over de resultaten van het laatste deel van het Computergrafiek project. Voor dit laatste deel heb ik voor het Globale Belichting traject gekozen. Het verslag vergelijkt de drie verschillende methodes waarmee gerendered werd op convergentiesnelheid in functie van het aantal stralen per pixel, de rekentijd en eventuele \emph{biases}. Er worden ook factoren besproken die de convergentietijd kunnen be\"invloeden, nl. de grootte van de lichtbron en de branching factor. De drie gebruikte methodes zijn Brute-Force Path Tracing, Directe Belichting en een hybride aanpak.

\paragraph{}
Om de convergentie, en bij extensie de convergentiesnelheid, te vergelijken gebruikt men de Root Mean Square Error. Ze is gedefini\"eerd als: $\text{RMSE} = \sqrt{\frac{\sum_{k=1}^{n} (L(p_k) - L(p_{k, \text{ref}}))^2}{n}}$ waar p een pixel is, n het aantal pixels en $L(.)$ een functie die een pixel omzet naar een re\"eel getal. De RMSE is een maat voor de gemiddelde fout (standaard afwijking) per pixel. Om de RMSE tussen verschillende scenario's (grote/kleine lichtbron) te vergelijken, wordt ze genormaliseerd met de gemiddelde waarde van het referentiebeeld. Dit resulteert in de variatie co\"effiecient van de RMSE, de CV(RMSE).

Voor de functie $L(p_k)$ heb ik gekozen om de pixel terug naar intensiteit om te zetten door te herschalen naar [0, 1] en de inverse gamma-correctie toe te passen. Door deze keuze is de RMSE een maat voor de fout van de intensiteit, i.p.v. $\text{intensiteit}^\frac{1}{\gamma}$.


\section{Path Tracer}

\subsection{Vergelijking Directe Belichting}
De duurste beelden voor de path tracer en de directe belichting staan hieronder naast elkaar. De path tracer gebruikt russian roullette om de paden te be\"eindigen en heeft dus geen bias. De directe belichting berekent geen indirecte belichting (en is dus duidelijk biased) en gebruikt maar 1 schaduw straal per straal. De grootste verschillen zijn:

\begin{itemize}
	\item De gemiddelde intensiteit van het beeld is voor de directe belichting lager dan voor de path tracer. Dit komt omdat indirecte belichting volledig genegeerd is.
	\item De vlakken die weggedraaid staan van de lichtbron, zijn volledig zwart. Dit is duidelijk te zien op het plafond en het voorvlak van de voorste balk.
	\item Bij de path tracer "bloedt" de kleur van de muren door op de rest van de sc\`ene.
	\item De directe belichting vergt minder werk (tijd) per straal. Als illustratie: op mijn laptop duurde het 35' met de path tracer voor 2048 stralen; bij directe belichting was dit 10'.
	\item De directe belichting convergeert sneller. Zie lager.
\end{itemize}


\begin{figure}[H]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{{../end/rrascending10.0k692.1699272677166m}.png}
		\caption{Cornell Box scene gerenderd met een brute-force path tracer met russian roullette en cosinus-sampling. 10k stralen per pixel.}
	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{{../end/direct/reference2.048k10.073894267333333m}.png}
		\caption{Cornell Box scene met eenvoudige directe belichting en shaduwstralen. Dit beeld is gerenderd met 1k stralen per pixel.}
	\end{minipage}
\end{figure}

\noindent
Een vergelijking van de Root Mean Squared Error ten opzichte van een referentie beeld, in functie van het aantal stralen per pixel, is te zien in figuur \ref{fig:rmse_path_direct}. Hier is duidelijk te zien dat de inherente variantie voor de path tracer hoger is dan die voor directe belichting. Dit komt omdat bij de directe belichting de enige bronnen van variantie de penumbra en hoeken $\approx 90\deg$ tussen de normaal en $\omega_o$ zijn. Het verschil in deze inherente variantie is duidelijk te zien op beelden die weinig stralen gebruiken.

Op de figuur staan twee datapunten aangeduid om $\frac{1}{\sqrt{N}}$ convergentie aan te duiden. Het aantal stralen van het ene datapunt op het volgende is verviervoudigd, dus verwachten we een verbetering van 2. In dit geval is het $\frac{0.04577}{0.0227} \approx 2.016$. Het is niet exact gelijk aan 2 omdat de $\frac{1}{\sqrt{N}}$ evenredigheid asymptotisch is voor $N\rightarrow\infty$.
In figuur \ref{fig:path_progressie} en \ref{fig:direct_progressie} is te zien hoe de beelden verbeteren in functie van het aantal stralen.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{rmse_path_direct.eps}
	\caption{RMSE t.o.v. een referentie in functie van het aantal stralen per pixel.}
	\label{fig:rmse_path_direct}
\end{figure}

\newcommand{\subf}[2]{ \subfloat[#2]{{\includegraphics[width=3cm]{#1} }} }


\subsection{Grootte van de Lichtbron}

In de bovenstaande beelden is een lichtbron gebruikt die groter is dan doorgaans bij de Cornell Box. Dit is zo gekozen zodat de inherente variantie lager is, en de convergentiesnelheid dus hoger.


In figuur \ref{fig:small_big} wordt de convergentiesnelheid tussen een grote en kleine lichtbron vergeleken. De variantie van de kleine lichtbron start lager maar stijgt eerst. Pas na 64 stralen begint hij terug te dalen. Deze stijging valt eveneens te verklaren door het feit dat de $\frac{1}{\sqrt{N}}$ afname asymptotisch is.

De oppervlakte van de grote lichtbron is 9 keer groter dan die van de kleine. In figuur \ref{fig:small_progressie} wordt de progressie van de beelden voor de kleine lichtbron getoond voor hetzelfde aantal stralen als in figuur \ref{fig:path_progressie} om te kunnen vergelijken. Hier is duidelijk te zien dat de grotere lichtbron sneller convergeert.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{rmse_big_small.eps}
	\caption{CV(RMSE) t.o.v. een referentie in functie van het aantal stralen per pixel.}
	\label{fig:small_big}
\end{figure}

\subsection{Branching Factor}
De branching factor verhoogt de convergentiesnelheid t.o.v. het aantal stralen per pixel. Of ze die ook verhoogt t.o.v. de tijd hangt af van de sc\`ene. In dit geval vergelijk ik de rendertijd van twee beelden waarvan de kwaliteit gelijkwaardig zou moeten zijn, nl. (b=1, N=512) en (b=3, N=128), zie figuur \ref{fig:branch_comparison}. Het eerste beeld duurde 8.8' en het tweede beeld 20.5'. Hieruit concludeer ik dat (na\"ieve) branching geen winst geeft voor deze eenvoudige sc\`ene.

De convergentiesnelheden zonder en met branching (branching factor 3 en een maximale diepte van 5) zijn te zien in figuur \ref{fig:rmse_branching}. Een progressie is te zien in figuur \ref{fig:branching_progressie}. Wat opvalt bij de progressie is dat het beeld met slechts 1 straal al herkenbaar is, en dat het heel snel convergeert.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{rmse_branching.eps}
	\caption{RMSE t.o.v. dezelfde referentie in functie van het aantal stralen per pixel. De zwarte streeplijn geeft aan dat het het beeld voor (b=1, N=512) en (b=3, N=128) vergelijkbaar in kwaliteit zijn.}
	\label{fig:rmse_branching}
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/rrascending0.512k8.788466546383333m}}}{b=1, N=512}%
	\subf{{{../end/branching/branching3b0.128k20.543696842733333m}}}{b=3, N=128}%
	\caption{Vergelijking van de kwaliteit van de twee beelden met een verschillende branching factor b en stralen per pixel N.}
	\label{fig:branch_comparison}%
\end{figure}

\subsection{Indirecte Belichting}
In figuur \ref{fig:direct_bounces} is de Cornell Box sc\`ene getoond met telkens enkel het directe licht na N bounces en hetzelfde aantal stralen per pixel. Hierop is te zien hoe de bijdrage van het indirecte licht afneemt met de diepte van het pad (de intensiteit daalt), maar toch relevant blijft. De oplichtende oppervlakken op diepte $N$ zijn de 'effectieve' lichtbronnen voor de indirecte belichting op diepte $N-1$.

\begin{figure}[H]%
	\centering
	\subf{{{../end/directAt/directAt0b0.1k0.23450607873333335m}}}{0}%
	\subf{{{../end/directAt/directAt1b0.1k1.81573078395m}}}{1}%
	\subf{{{../end/directAt/directAt2b0.1k1.5202384521333334m}}}{2}%
	\subf{{{../end/directAt/directAt3b0.1k1.4478653135666666m}}}{3}%
	\subf{{{../end/directAt/directAt4b0.1k2.557572799m}}}{4}%
	\caption{Cornell Box waar enkel het licht na N bounces is weergeven.}%
	\label{fig:direct_bounces}%
\end{figure}

\section{Hybride}
De hybride aanpak is een techniek om de variantie te verlagen door importance sampling van de lichtbronnen en de hemisfeer te combineren. Figuur \ref{fig:rmse_hybrid} vergelijkt zijn convergentiesnelheid met zowel de brute-force als de branching aanpak. Wat opmerkelijk is, is dat de hybride aanpak zelfs sneller convergeert dan de branching aanpak voor deze sc\`ene. Op mijn laptop duurt het 16' om met brute-force path tracing 1024 stralen per pixel uit te rekenen. De hybride aanpak duurt 2.2' om een gelijkwaardig beeld uit te rekenen met maar 128 stralen per pixel. Deze twee beelden zijn te zien in figuur \ref{fig:hybrid_comparison}. Een progressie van deze aanpak is te zien op figuur \ref{fig:hybrid_progressie}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{rmse_hybrid.eps}
	\caption{Vergelijking van de convergentiesnelheid voor de brute-force path tracer met en zonder branching, en de hybride aanpak. De zwarte streeplijn geeft een gelijkwaardig kwaliteitsniveau aan.}
	\label{fig:rmse_hybrid}
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/rrascending1.024k15.964094286833333m}}}{Brute-Force N=1024}%
	\subf{{{../end/hybrid/hybrid0.128k2.24332931055m}}}{Hybride N=128}%
	\caption{Vergelijking van de kwaliteit van de twee beelden met een verschillende branching factor b en stralen per pixel N.}
	\label{fig:hybrid_comparison}%
\end{figure}

\section{Caustics}
In figuur \ref{fig:caustic_comparison} worden twee koffietassen vergeleken. Het linkerbeeld is een koffietas met een exact spiegel materiaal en een witte area light achter de camera, gerenderd met de brote-force aanpak. Het rechter beeld is een echte foto waar de zon achter de camera staat. Beide beelden vertonen een maanvormige caustic op de bodem van de tas. De convergentie voor deze scene duurt langer omdat er veel bounces nodig zijn om een unbiased beeld te verkrijgen, zie figuur \ref{fig:rmse_caustics} voor een vergelijking met de Cornell Box. Een progressie is te zien in figuur \ref{fig:caustic_progressie}.

\begin{figure}[H]%
	\centering
	\subf{{{../end/mug/mug2.0k60.40317227925m}}}{Render van een koffietas. 2048 Stralen per pixel.}%
	\subf{{{../end/mugref}}}{Foto van een koffietas.}%
	\caption{}
	\label{fig:caustic_comparison}%
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{rmse_caustics.eps}
	\caption{Vergelijking van de convergentiesnelheid voor de hybride aanpak op de koffietas en de Cornell Box.}
	\label{fig:rmse_caustics}
\end{figure}

\section{Conclusie}
Directe belichting met schaduwstralen is snel en gemakkelijk te implementeren. Dit heeft het grote nadeel dat het een significante bias heeft omdat het belangrijke effecten van indirecte belichting mist. De eenvoudige oplossing is om een brute-force path tracer te gebruiken. Dit resulteert in een unbiased beeld maar is erg traag als er geen variantie reductie technieken worden toegepast. Een combinatie van de directe belichting en de path tracer in een hybride methode zorgt voor een enorme versnelling. Toch zullen er nog sc\`enes zijn waar ze traag convergeert. Hier moet dan gekeken worden naar andere technieken zoals bidirectionele path tracing of Metropolis light transport.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{../end/car/composite}
	\caption{Foto van een auto dat gebruikt maakt van o.a. Fresnel reflectie/refractie (ruiten), omgevingslichtbron, texturen en acceleratiestructuren.}
\end{figure}


\section{Progressies}
\begin{figure}[H]%
	\centering
	\subf{{{../end/rrascending0.001k0.021341364933333332m}}}{1}%
	\subf{{{../end/rrascending0.032k0.386077658m}}}{32}%
	\subf{{{../end/rrascending0.256k3.9156613981333335m}}}{256}%
	\subf{{{../end/rrascending2.048k35.22650692276667m}}}{2048}%
	\subf{{{../end/rrascending8.192k308.1307705257833m}}}{8192}%
	\caption{Progressie van de Cornell Box scene met de brute-force path tracer.}%
	\label{fig:path_progressie}%
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/direct/direct0.001k0.0196476438m}}}{1}%
	\subf{{{../end/direct/direct0.032k0.15329985665m}}}{32}%
	\subf{{{../end/direct/direct0.256k1.2313848105m}}}{256}%
	\subf{{{../end/direct/reference2.048k10.073894267333333m}}}{2048}%
	\caption{Progressie van de Cornell Box scene met directe belichting.}%
	\label{fig:direct_progressie}%
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/small/rrascending0.001k0.037801545316666664m}}}{1}%
	\subf{{{../end/small/rrascending0.032k0.971779138m}}}{32}%
	\subf{{{../end/small/rrascending0.256k131.50070259123333m}}}{256}%
	\subf{{{../end/small/rrascending2.048k59.14490982365m}}}{2048}%
	\caption{Progressie van de Cornell Box scene met de brute-force path tracer voor een kleinere lichtbron.}%
	\label{fig:small_progressie}%
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/branching/branching3b0.001k0.14279041065m}}}{1}%
	\subf{{{../end/branching/branching3b0.008k1.1879305112166667m}}}{8}%
	\subf{{{../end/branching/branching3b0.064k10.006859705m}}}{64}%
	\subf{{{../end/branching/branching3b0.512k81.52543559315m}}}{512}%
	\caption{Progressie van de Cornell Box scene met de brute-force path tracer met een branching factor van 3 en een maximale diepte van 5.}%
	\label{fig:branching_progressie}%
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/hybrid/hybrid0.001k0.019920115833333335m}}}{1}%
	\subf{{{../end/hybrid/hybrid0.008k0.12385172298333333m}}}{8}%
	\subf{{{../end/hybrid/hybrid0.128k2.24332931055m}}}{128}%
	\subf{{{../end/reference2.0k14.9350952141m}}}{2048}%
	\caption{Progressie van de Cornell Box scene met de hybride aanpak.}
	\label{fig:hybrid_progressie}%
\end{figure}
\begin{figure}[H]%
	\centering
	\subf{{{../end/mug/mug0.001k0.06459967768333333m}}}{1}%
	\subf{{{../end/mug/mug0.008k0.6135682430666667m}}}{8}%
	\subf{{{../end/mug/mug0.128k12.745067180716667m}}}{128}%
	\subf{{{../end/mug/mug2.0k60.40317227925m}}}{2048}%
	\caption{Progressie van een koffie tas scene met de brute-force aanpak.}
	\label{fig:caustic_progressie}%
\end{figure}
\end{document}
