figure(); hold on;
xlim([-1.5, 1.5]);
ylim([0, 1.5]);
pbaspect([2 1 1])
theta = linspace(0, pi, 100);
plot(0, 0, 'k.');
plot(cos(theta), sin(theta), 'k-');
plot([-1.5, 1.5], [0, 0], 'k');

theta2 = linspace(1, 1.5, 100);
plot(1.1*cos(theta2), 1.1*sin(theta2), 'r-');
plot([0, 1.1*cos(theta2(1))], [0, 1.1*sin(theta2(1))], 'b--');
plot([0, 1.1*cos(theta2(end))], [0, 1.1*sin(theta2(end))], 'b--');


% this will remove the lines/tick marks
     box off;
     set(gca,'xcolor',get(gcf,'color'));
     set(gca,'xtick',[]);
     set(gca,'ycolor',get(gcf,'color'));
     set(gca,'ytick',[]);
% ... same for <y...>/<z...> pairs
% and this would set the axis color
% ... to the figures bg color
     set(gca,'color',get(gcf,'color'));
     pause;
% ... now reset the label(s)
     set(get(gca,'xlabel'),'color',[0 0 0]);
