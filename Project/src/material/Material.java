package material;

import BSDF.BRDF;
import film.RGBSpectrum;

/**
 * Created by Menno on 28/02/2017.
 */
public class Material {
    public RGBSpectrum diffuse;
    public BRDF brdf;

    public Material(RGBSpectrum diff, BRDF brdf) {
        this.diffuse = diff;
        this.brdf = brdf;
    }
}
