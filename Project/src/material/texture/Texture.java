package material.texture;

import film.RGBSpectrum;

/**
 * Created by Menno on 28/02/2017.
 */
public interface Texture {
    RGBSpectrum at(double u, double v);
}
