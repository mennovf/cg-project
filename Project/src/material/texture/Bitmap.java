package material.texture;

import film.RGBSpectrum;
import math.Util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Menno on 28/02/2017.
 */
public class Bitmap implements Texture {
    public enum Filtering {
        NEAREST
    }

    public enum Access {
        THROW,
        TILED
    }

    private int tiled_X(int x) {
        return Util.mod(x, Bitmap.this.bitmap.getWidth());
    }

    private int tiled_Y(int y) {
        return Util.mod(y, Bitmap.this.bitmap.getHeight());
    }

    public Filtering mode;
    public Access access;
    public BufferedImage bitmap;

    public Bitmap(String path, Filtering m) {
        this(path, m, Access.THROW);
    }

    public Bitmap(String path, Filtering m, Access a) {
        this.mode = m;
        this.access = a;
        try {
            bitmap = ImageIO.read(new File(path));
        } catch (IOException e) {
            System.err.println("Unable to read/find file: " + path);
        }
    }

    @Override
    public RGBSpectrum at(double u, double v) {
        switch (this.mode) {
            case NEAREST:
                int x = (int)Math.round(u * (bitmap.getWidth()-1));
                int y = (int)Math.round((1-v) * (bitmap.getHeight()-1));

                // Do access sanitization
                switch (this.access) {
                    case TILED: {
                        x = tiled_X(x);
                        y = tiled_Y(y);
                        break;
                    }
                    default: break;
                }

                return new RGBSpectrum(new Color(bitmap.getRGB(x, y)));
        }
        // Should never be reached!
        assert false;
        return null;
    }
}
