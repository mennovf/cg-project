package material.texture;

import film.RGBSpectrum;

/**
 * Created by Menno on 28/02/2017.
 */
public class Constant implements Texture {
    public RGBSpectrum color;

    public Constant(RGBSpectrum c) {
        this.color = c;
    }

    @Override
    public RGBSpectrum at(double u, double v) {
        return color;
    }
}
