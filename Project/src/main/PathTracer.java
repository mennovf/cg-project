package main;

import BSDF.BRDF;
import entity.Entity;
import film.RGBSpectrum;
import light.Light;
import math.*;

import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 30/03/2017.
 */
public class PathTracer {
    protected int max_depth;
    protected int branching_factor;
    protected boolean do_russian_roulette;
    protected boolean direct;
    protected Random random;

    public PathTracer(int max_depth, int branching_factor, boolean rr, boolean direct) {
        assert branching_factor >= 1;
        assert max_depth >= 1;

        this.max_depth = max_depth;
        this.branching_factor = branching_factor;
        this.do_russian_roulette = rr;
        this.direct = direct;
    }

    public RGBSpectrum trace(Random random, Ray ray, Scene scene) {
        return this.trace(random, ray, 0, scene);
    }

    public RGBSpectrum trace(Random random, Ray ray, int current_depth, Scene scene) {
        if (current_depth >= this.max_depth) {
            return RGBSpectrum.BLACK;
        }

        RGBSpectrum color = RGBSpectrum.BLACK;

        // Check intersections with the light source if the tracing method is indirect or if
        // this is the first iteration of direct lighting
        Light.LightSample best_lightsample = null;
        double best_light_distance = Double.POSITIVE_INFINITY;
        if (!this.direct || current_depth == 0){
            for (Light light: scene.lights) {
                Optional<Light.LightSample> olightsample = light.intersect(ray);
                if (olightsample.isPresent()) {
                    Light.LightSample lightsample = olightsample.get();
                    double distance = ray.origin.subtract(lightsample.point).length();
                    if (best_lightsample == null || (distance <= best_light_distance)) {
                        best_lightsample = lightsample;
                        best_light_distance = distance;
                    }
                }
            }
        }


        Optional<Entity.IntersectionInfo> isect = scene.intersect(ray);
        if (!isect.isPresent() || (best_lightsample != null && isect.get().t() >= best_light_distance)) return (best_lightsample != null) ? best_lightsample.power : RGBSpectrum.BLACK;
        Entity.IntersectionInfo iinfo = isect.get();
        Transformation local_basis = (new OrthonormalBasis(iinfo.intersection.normal)).asTransformation();


        if (this.direct) {
            for (Light light : scene.lights) {
                Light.LightSample light_sample = light.sample(random, iinfo.intersection.intersection);

                //Check visibility
                final Vector to_light = light_sample.point.subtract(iinfo.intersection.intersection);
                final Vector omega_light = to_light.normalized();
                final Ray shadow_ray = new Ray(iinfo.intersection.intersection.add(iinfo.intersection.normal.scale(1e-5)), omega_light);

                Optional<Entity.IntersectionInfo> sisect = scene.intersect(shadow_ray);
                if (!sisect.isPresent() || sisect.get().intersection.t >= to_light.length()) {
                    double f = iinfo.material.brdf.f(local_basis.transformInverse(ray.direction), local_basis.transformInverse(omega_light));
                    double cos_point = Vector.dot(iinfo.intersection.normal, omega_light);
                    cos_point = Util.clamp(cos_point, 0, 1);
                    color = color.add(light_sample.power.scale(f*cos_point*light_sample.g).divide(light_sample.p));
                }
            }
        }

        for (int i = 0; i < this.branching_factor; ++i) {
            Vector in = local_basis.transformInverse(ray.direction).normalized();

            BRDF.BRDFSample brdfsample = iinfo.material.brdf.sample(random, in);
            final Ray out_ray = new Ray(iinfo.intersection, local_basis.transform(brdfsample.direction), 1e-5);
            final double cos = Math.abs(brdfsample.direction.z);
            RGBSpectrum ray_color =  this.trace(random, out_ray, current_depth+1, scene).scale(brdfsample.f*cos / (brdfsample.p * this.branching_factor));

            if (this.do_russian_roulette) {
                final double throughput = brdfsample.f * cos * iinfo.material.diffuse.max();
                if (random.nextDouble() > throughput) {ray_color = RGBSpectrum.BLACK;}
                else {ray_color = ray_color.divide(throughput);}
            }

            color = color.add(ray_color);
        }
        color = color.multiply(iinfo.material.diffuse);

        return color;
    }
}
