package main;

import BSDF.Fresnel;
import BSDF.Lambert;
import BSDF.Mirror;
import acceleration.BVH;
import camera.Camera;
import camera.PerspectiveCamera;
import entity.Entity;
import entity.Primitive;
import film.RGBSpectrum;
import light.AreaLight;
import light.EnvironmentLight;
import light.Light;
import light.PointLight;
import material.texture.Bitmap;
import material.texture.Constant;
import material.texture.Texture;
import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;
import shape.Sphere;
import shape.TriangleMesh;

import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by Menno on 10/03/2017.
 */
public class Scene {
    public Collection<Light> lights;
    public BVH<Entity, Entity.IntersectionInfo> entities;
    public Camera camera;

    public Scene(Collection<Entity> es, Collection<Light> ls) {
        this.entities = new BVH<>(es, 1);
        this.lights = ls;
    }

    Optional<Entity.IntersectionInfo> intersect(Ray r) {
        return this.entities.intersect(r);
    }

    static Scene CornellBox(int width, int height) {
        ArrayList<Entity> entities = new ArrayList<>();
        ArrayList<Light> lights = new ArrayList<>();
        Texture beige = new Constant(new RGBSpectrum(0.73));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "backwall.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "ceiling.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "floor.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, new Constant(new RGBSpectrum(1,0,0)), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "leftwall.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, new Constant(new RGBSpectrum(0,1,0)), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "rightwall.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "shortbox.obj"), TriangleMesh.Type.BVH), new Lambert()));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "tallbox.obj"), TriangleMesh.Type.BVH), new Lambert()));
        lights.add(new AreaLight(new RGBSpectrum(10), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "light.obj"), TriangleMesh.Type.BVH), Transformation.scale(3, 1, 3), AreaLight.Directional.UNI));

        Scene scene = new Scene(entities, lights);
        scene.camera = new PerspectiveCamera(width, height, new Point(0, 1, 2.7), new Vector(0, 0, -1), new Vector(0., 1., 0.), 60);
        return scene;
    }

    static Scene CoffeeMug(int width, int height) {
        ArrayList<Entity> entities = new ArrayList<>();
        ArrayList<Light> lights = new ArrayList<>();
        Transformation T = Transformation.translate(0, -0.1, 0).append(Transformation.rotateX(70)).append(Transformation.rotateY(150)).append(Transformation.scale(0.05));
        Transformation T_light = Transformation.translate(0, 0, 5).append(Transformation.rotateX(-90));
        entities.add(new Primitive(T, new Constant(new RGBSpectrum(1)), new TriangleMesh(FileSystems.getDefault().getPath("models/", "mug.obj"), TriangleMesh.Type.BVH), new Mirror()));
        lights.add(new AreaLight(new RGBSpectrum(100), new TriangleMesh(FileSystems.getDefault().getPath("models", "plane.obj"), TriangleMesh.Type.BVH), T_light, AreaLight.Directional.BI));
//        lights.add(new PointLight(new RGBSpectrum(100), new Point(0, 0, 5)));

        Scene scene = new Scene(entities, lights);
        scene.camera = new PerspectiveCamera(width, height, new Point(0, 0.05, 0.9), new Vector(0, 0, -1), new Vector(0, 1., 0.), 60);
        return scene;
    }

    static Scene Fresnel(int width, int height) {
        ArrayList<Entity> entities = new ArrayList<>();
        ArrayList<Light> lights = new ArrayList<>();
        Texture beige = new Constant(new RGBSpectrum(0.73));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "backwall.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "ceiling.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "floor.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, new Constant(new RGBSpectrum(1,0,0)), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "leftwall.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, new Constant(new RGBSpectrum(0,1,0)), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "rightwall.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, beige, new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "tallbox.obj"), TriangleMesh.Type.BVH), new Lambert()));
        entities.add(new Primitive(Transformation.translate(0, 0.51, 0).append(Transformation.scale(0.5)), new Constant(new RGBSpectrum(1)), new Sphere(), new Fresnel(1.52)));
        lights.add(new AreaLight(new RGBSpectrum(10), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "light.obj"), TriangleMesh.Type.BVH), Transformation.scale(3, 1, 3), AreaLight.Directional.BI));

        Scene scene = new Scene(entities, lights);
        scene.camera = new PerspectiveCamera(width, height, new Point(0, 1, 2.7), new Vector(0, 0, -1), new Vector(0., 1., 0.), 60);
        return scene;
    }

    static Scene Car(int width, int height) {
        ArrayList<Entity> entities = new ArrayList<>();
        ArrayList<Light> lights = new ArrayList<>();
        Texture road = new Bitmap("models/car/road.jpg", Bitmap.Filtering.NEAREST, Bitmap.Access.TILED);
        Texture car = new Constant(new RGBSpectrum(0.9, 0 , 0));
        Texture sky = new Bitmap("models/car/sky.jpg", Bitmap.Filtering.NEAREST, Bitmap.Access.TILED);
        Texture glass = new Constant(new RGBSpectrum(1));
        Texture theapot = new Constant(new RGBSpectrum(0, 0, 1));
        Texture interior = new Constant(new RGBSpectrum(0.01));
        Texture wheel_rubber = new Bitmap("models/car/wheel.jpg", Bitmap.Filtering.NEAREST, Bitmap.Access.TILED);
        Texture wheel_rim = new Constant(new RGBSpectrum(0.9));
        Transformation T = Transformation.translate(0.5, 0, 0);
//        entities.add(new Primitive(Transformation.IDENTITY, new Constant(new RGBSpectrum(0.81)), new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "ground.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, road, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "road1.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, road, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "road2.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, road, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "road3.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(Transformation.IDENTITY, road, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "road4.obj"), TriangleMesh.Type.BVH)));
//        entities.add(new Primitive(T, wheels, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel1.obj"), TriangleMesh.Type.BVH)));
//        entities.add(new Primitive(T, wheels, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel2.obj"), TriangleMesh.Type.BVH)));
//        entities.add(new Primitive(T, wheels, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel3.obj"), TriangleMesh.Type.BVH)));
//        entities.add(new Primitive(T, wheels, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel4.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(T, wheel_rim, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel_rf_rim.obj"), TriangleMesh.Type.BVH), new Mirror()));
        entities.add(new Primitive(T, wheel_rubber, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel_rf_rubber.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(T, wheel_rim, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel_rb_rim.obj"), TriangleMesh.Type.BVH), new Mirror()));
        entities.add(new Primitive(T, wheel_rubber, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "wheel_rb_rubber.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(T, interior, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "interior.obj"), TriangleMesh.Type.BVH)));
        entities.add(new Primitive(T, car, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "chassis.obj"), TriangleMesh.Type.BVH), new Mirror()));
        entities.add(new Primitive(T, glass, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "windows.obj"), TriangleMesh.Type.BVH), new Fresnel(1.52)));
        entities.add(new Primitive(T, theapot, new TriangleMesh(FileSystems.getDefault().getPath("models/car/out", "driver.obj"), TriangleMesh.Type.BVH)));
//        lights.add(new AreaLight(new RGBSpectrum(100), new TriangleMesh(FileSystems.getDefault().getPath("models/cornell/Original", "light.obj"), TriangleMesh.Type.BVH), Transformation.translate(0, 10, 0).append(Transformation.scale(100, 1, 100)), AreaLight.Directional.BI));
        lights.add(new EnvironmentLight(sky, 1));

        Scene scene = new Scene(entities, lights);
        scene.camera = new PerspectiveCamera(width, height, new Point(-2.2, 0.552, -1.5), new Point(0, 0, 0), new Vector(0., 1., 0.), 45);
        return scene;
    }
}
