package main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import camera.Camera;
import film.RGBSpectrum;
import math.*;
import sampling.Sample;
import film.FrameBuffer;
import film.Tile;
import gui.ProgressReporter;
import gui.RenderFrame;

/**
 * Entry point of your renderer.
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Renderer {
	/**
	 * Entry point of your renderer.
	 * 
	 * @param arguments
	 *            command line arguments.
	 * @throws InterruptedException
	 * @throws InvocationTargetException
	 */
	public static void main(String[] arguments) {
        int width = 320;
        int height = 320;
		double sensitivity = 1.0;
		double gamma = 2.2;
		boolean gui = true;
		boolean quiet = false;
		Point origin = new Point(0, 0, 0);
		Point destination = new Point(0, 0, 1);
		Vector lookup = new Vector(0, 1, 0);
		double fov = 60;
		String filename = "end/car/car";
		int samples_amount = 10;
        double inv_samples_amount = 1.0 / samples_amount;

		/**********************************************************************
		 * Parse the command line arguments
		 *********************************************************************/

		for (int i = 0; i < arguments.length; ++i) {
			if (arguments[i].startsWith("-")) {
				String flag = arguments[i];

				try {
					if (flag.equals("-width"))
						width = Integer.parseInt(arguments[++i]);
					else if (flag.equals("-height"))
						height = Integer.parseInt(arguments[++i]);
					else if (flag.equals("-gui"))
						gui = Boolean.parseBoolean(arguments[++i]);
					else if (flag.equals("-quiet"))
						quiet = Boolean.parseBoolean(arguments[++i]);
					else if (flag.equals("-sensitivity"))
						sensitivity = Double.parseDouble(arguments[++i]);
                    else if (flag.equals("-samples")) {
                        samples_amount = Integer.parseInt(arguments[++i]);
                        inv_samples_amount = 1. / samples_amount;
                    }
					else if (flag.equals("-gamma"))
						gamma = Double.parseDouble(arguments[++i]);
					else if (flag.equals("-origin")) {
						double x = Double.parseDouble(arguments[++i]);
						double y = Double.parseDouble(arguments[++i]);
						double z = Double.parseDouble(arguments[++i]);
						origin = new Point(x, y, z);
					} else if (flag.equals("-destination")) {
						double x = Double.parseDouble(arguments[++i]);
						double y = Double.parseDouble(arguments[++i]);
						double z = Double.parseDouble(arguments[++i]);
						destination = new Point(x, y, z);
					} else if (flag.equals("-lookup")) {
						double x = Double.parseDouble(arguments[++i]);
						double y = Double.parseDouble(arguments[++i]);
						double z = Double.parseDouble(arguments[++i]);
						lookup = new Vector(x, y, z);
					} else if (flag.equals("-fov")) {
						fov = Double.parseDouble(arguments[++i]);
					} else if (flag.equals("-output")) {
						filename = arguments[++i];
					} else if (flag.equals("-help")) {
						System.out
								.println("usage: java -jar cgpracticum.jar\n"
										+ "  -width <integer>      width of the image\n"
										+ "  -height <integer>     height of the image\n"
										+ "  -sensitivity <double> scaling factor for the radiance\n"
                                        + "  -samples <integer>    the amount of samples per pixel\n"
										+ "  -gamma <double>       gamma correction factor\n"
										+ "  -origin <point>       origin for the camera\n"
										+ "  -destination <point>  destination for the camera\n"
										+ "  -lookup <vector>      up direction for the camera\n"
										+ "  -output <string>      filename for the image\n"
										+ "  -gui <boolean>        whether to start a graphical user interface\n"
										+ "  -quiet <boolean>      whether to print the progress bar");
						return;
					} else {
						System.err.format("unknown flag \"%s\" encountered!\n",
								flag);
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					System.err.format("could not find a value for "
							+ "flag \"%s\"\n!", flag);
				}
			} else
				System.err.format("unknown value \"%s\" encountered! "
						+ "This will be skipped!\n", arguments[i]);
		}

		/**********************************************************************
		 * Validate the input
		 *********************************************************************/

		if (width <= 0)
			throw new IllegalArgumentException("the given width cannot be "
					+ "smaller than or equal to zero!");
		if (height <= 0)
			throw new IllegalArgumentException("the given height cannot be "
					+ "smaller than or equal to zero!");
		if (gamma <= 0)
			throw new IllegalArgumentException("the gamma cannot be "
					+ "smaller than or equal to zero!");
		if (sensitivity <= 0)
			throw new IllegalArgumentException("the sensitivity cannot be "
					+ "smaller than or equal to zero!");
        if (samples_amount <= 0)
            throw new IllegalArgumentException("The amount of samples cannot be less than or equal to zero!");
		if (fov <= 0)
			throw new IllegalArgumentException("the field of view cannot be "
					+ "smaller than or equal to zero!");
		if (fov >= 180)
			throw new IllegalArgumentException("the field of view cannot be "
					+ "larger than or equal to 180!");
		if (filename.isEmpty())
			throw new IllegalArgumentException("the filename cannot be the "
					+ "empty string!");

		/**********************************************************************
		 * Initialize the camera and graphical user interface
		 *********************************************************************/

//		Camera camera = new PerspectiveCamera(width, height,
//				new Point(0, 1, 0), destination, lookup, fov);

		// initialize the frame buffer
		final FrameBuffer colour = new FrameBuffer(width, height);
        final FrameBuffer depth = new FrameBuffer(width, height);
        final FrameBuffer normals = new FrameBuffer(width, height);

		// initialize the progress reporter
		final ProgressReporter reporter = new ProgressReporter("Rendering", 40,
				width * height, quiet);

		// initialize the graphical user interface
		RenderFrame userinterface;
		if (gui) {
			try {
				userinterface = RenderFrame.buildRenderFrame(colour, depth, normals, gamma,
						sensitivity);
				reporter.addProgressListener(userinterface);
			} catch (Exception e) {
				userinterface = null;
			}
		} else
			userinterface = null;

		final RenderFrame frame = userinterface;

		/**********************************************************************
		 * Initialize the scene
		 *********************************************************************/

        final long start_time = System.nanoTime();
        Scene scene = Scene.Car(width, height);
        final Camera camera = scene.camera;
//		final PathTracer path_tracer = new PathTracer(8, 1, false, true);
		final PathTracer path_tracer = new PathTracer(10, 1, false, false);

        // For debugging
        if (userinterface != null) {
            userinterface.color_panel.click_listener = (int x, int y) -> {
                System.out.println("Clicked at: " + Integer.toString(x) + " " + Integer.toString(y));
                Ray ray = camera.generateRay(new Sample(x + 0.5, y + 0.5));

                scene.intersect(ray);
				path_tracer.trace(new Random(), ray, scene);
            };
        }

		/**********************************************************************
		 * Multi-threaded rendering of the scene
		 *********************************************************************/
        // Output the intermediary images every n rays.
		final ExecutorService service = Executors.newFixedThreadPool(Runtime
				.getRuntime().availableProcessors());

        final int samples = samples_amount;
        final double inv_samples = inv_samples_amount;
		// subdivide the buffer in equal sized tiles
		for (final Tile tile : colour.subdivide(64, 64)) {
			// create a thread which renders the specific tile
			Thread thread = new Thread() {
				@Override
				public void run() {
					Random random = new Random(tile.yStart + tile.getWidth() * tile.xStart + samples*(tile.getWidth()*tile.getHeight()));
					try {
						// iterate over the contents of the tile
						for (int y = tile.yStart; y < tile.yEnd; ++y) {
							for (int x = tile.xStart; x < tile.xEnd; ++x) {
								RGBSpectrum pixel_value = new RGBSpectrum(0);
								for (int sample_nr = 0; sample_nr < samples; ++sample_nr) {
									// create a ray through the center of the pixel.
									Ray ray = camera.generateRay(new Sample(x + 0.5, y + 0.5, random));

									RGBSpectrum Li = path_tracer.trace(random, ray, scene).scale(inv_samples);
									pixel_value = pixel_value.add(Li);
								}

								colour.getPixel(x, y).add(pixel_value);
							}
						}

						// update the graphical user interface
						if (frame != null) {
							frame.color_panel.finished(tile);
							frame.depth_panel.finished(tile);
							frame.normals_panel.finished(tile);
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.exit(1);
					} catch (StackOverflowError e) {
						e.printStackTrace();
						System.exit(1);
					} catch (OutOfMemoryError e) {
						e.printStackTrace();
						System.exit(1);
					}

					// update the progress reporter
					reporter.update(tile.getWidth() * tile.getHeight());

				}
			};
			service.submit(thread);
		}

		// signal the reporter that rendering has started
		reporter.start();

		// execute the threads
		service.shutdown();

		// wait until the threads have finished
		try {
			service.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		final long end_time = System.nanoTime();

		// signal the reporter that the task is done
		reporter.done();
		System.out.println("Total: " + (end_time-start_time)/1.0e9 + "s");

		/**********************************************************************
		 * Export the result
		 *********************************************************************/

		// Use the sensitivity to scale the image to it's current representation
		BufferedImage result = colour.toBufferedImage(sensitivity, gamma);
		try {
			String full_filename = filename + samples_amount/1000.0 + "k" + (end_time-start_time)/(1.0e9*60) + "m" + ".png";
			ImageIO.write(result, "png", new File(full_filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
