package main;

import BSDF.BRDF;
import entity.Entity;
import film.RGBSpectrum;
import light.Light;
import math.*;

import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 7/04/2017.
 */
public class DirectAt {

    int max_depth;
    public DirectAt(int max_depth) {this.max_depth = max_depth;}

    public RGBSpectrum trace(Random r, Ray ray, Scene scene) {return this.trace(r, ray, 1, scene);}
    public RGBSpectrum trace(Random random, Ray ray, int current_depth, Scene scene) {
        Optional<Entity.IntersectionInfo> isect = scene.intersect(ray);
        if (!isect.isPresent()) return RGBSpectrum.BLACK;
        Entity.IntersectionInfo iinfo = isect.get();
        Transformation local_basis = (new OrthonormalBasis(iinfo.intersection.normal)).asTransformation();

        RGBSpectrum color = RGBSpectrum.BLACK;

        if (this.max_depth == 0){
            Light.LightSample best_lightsample = null;
            double best_distance = Double.POSITIVE_INFINITY;

            for (Light light: scene.lights) {
                Optional<Light.LightSample> olightsample = light.intersect(ray);
                if (olightsample.isPresent()) {
                    Light.LightSample lightsample = olightsample.get();
                    double distance = ray.origin.subtract(lightsample.point).length();
                    if (distance <= iinfo.t() && (best_lightsample == null || (distance <= best_distance))) {
                        best_lightsample = lightsample;
                        best_distance = distance;
                    }
                }
            }

            if (best_lightsample != null) {
                // TODO: might need conversion to Le
                color = best_lightsample.power;
            }
            return color;
        }
        if (current_depth == max_depth) {
            for (Light light : scene.lights) {
                Light.LightSample light_sample = light.sample(random, iinfo.intersection.intersection);

                //Check visibility
                final Vector to_light = light_sample.point.subtract(iinfo.intersection.intersection);
                final Vector omega_light = to_light.normalized();
                final Ray shadow_ray = new Ray(iinfo.intersection.intersection.add(iinfo.intersection.normal.scale(1e-5)), omega_light);

                Optional<Entity.IntersectionInfo> sisect = scene.intersect(shadow_ray);
                if (!sisect.isPresent() || sisect.get().intersection.t >= to_light.length()) {
                    double f = iinfo.material.brdf.f(local_basis.transformInverse(ray.direction), local_basis.transformInverse(omega_light));
                    double R2 = to_light.lengthSquared();
                    double cos_point = Vector.dot(iinfo.intersection.normal, omega_light);
                    cos_point = Util.clamp(cos_point, 0, 1);
                    color = color.add(light_sample.power.scale(f*cos_point).divide(R2 * light_sample.p));
                }
            }
        }

        Vector in = local_basis.transformInverse(ray.direction);

        BRDF.BRDFSample brdfsample = iinfo.material.brdf.sample(random, in);
        final Ray out_ray = new Ray(iinfo.intersection.intersection, local_basis.transform(brdfsample.direction));
        RGBSpectrum ray_color =  this.trace(random, out_ray, current_depth+1, scene).scale(brdfsample.f*brdfsample.direction.z / brdfsample.p);

        color = color.add(ray_color);
        color = color.multiply(iinfo.material.diffuse);

        return color;
    }
}
