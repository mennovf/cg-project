package entity;

import BSDF.BRDF;
import BSDF.Lambert;
import acceleration.AABB;
import film.RGBSpectrum;
import material.Material;
import material.texture.Texture;
import math.*;
import shape.Shape;

import java.util.Optional;

/**
 * Created by Menno on 15/02/2017.
 */
public class Primitive extends Entity {
    public Shape shape;

    public Primitive(Transformation transform, Texture diffuse, Shape shape, BRDF brdf) {
        super(transform, diffuse, brdf);
        this.shape = shape;
    }
    public Primitive(Transformation transform, Texture diffuse, Shape shape) {
        this(transform, diffuse, shape, new Lambert());
    }

    @Override
    public AABB AABB() {
        AABB ret = new AABB();
        AABB saabb = this.shape.AABB();
        for (double x : new double[]{0, 1}) {
            for (double y : new double[]{0, 1}) {
                for (double z : new double[]{0, 1}) {
                    ret.expand(transform.transform(new Point(Util.lerp(saabb.pMin.x, saabb.pMax.x, x),
                                                             Util.lerp(saabb.pMin.y, saabb.pMax.y, y),
                                                             Util.lerp(saabb.pMin.z, saabb.pMax.z, z))));
                }
            }
        }
        return ret;
    }

    @Override
    public Optional<IntersectionInfo> intersect(Ray ray) {
        final Ray local_ray = transform.transformInverse(ray);
        Optional<Shape.Intersection> isect = shape.intersect(local_ray);
        ray.increment_tests(local_ray.tests);

        if (!isect.isPresent()) {
            return Optional.empty();
        }

        IntersectionInfo iinfo = new IntersectionInfo();
        // Extract shape intersection and transform from "shape" to "world"
        iinfo.intersection = isect.get();
        iinfo.intersection.normal = transform.transformAsNormal(iinfo.intersection.normal).normalized();
        iinfo.intersection.intersection = transform.transform(iinfo.intersection.intersection);
        iinfo.intersection.t = iinfo.intersection.intersection.subtract(ray.origin).length() / ray.direction.length();

        // Insert material info
        final double u = iinfo.intersection.u;
        final double v = iinfo.intersection.v;
        iinfo.material = new Material(diffuse.at(u, v), this.brdf);

        // Add reference to the entity
        iinfo.entity = this;

        return Optional.of(iinfo);
    }
}
