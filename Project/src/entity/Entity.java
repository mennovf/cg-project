package entity;

import BSDF.BRDF;
import acceleration.BVH;
import acceleration.Boundable;
import film.RGBSpectrum;
import material.Material;
import math.Ray;
import math.Transformation;
import shape.Shape;
import material.texture.Texture;

import java.util.Optional;

/**
 * Created by Menno on 15/02/2017.
 */
public abstract class Entity implements Boundable<Entity.IntersectionInfo> {
    public Transformation transform;
    public Texture diffuse;
    public BRDF brdf;

    public Entity(Transformation transform, Texture d, BRDF brdf) {
        this.transform = transform;
        this.diffuse = d;
        this.brdf = brdf;
    }

    public class IntersectionInfo implements BVH.BasicIntersection {
        public Shape.Intersection intersection;
        public Material material;
        public Entity entity;

        @Override
        public double t() {
            return intersection.t();
        }
    }

    @Override
    public abstract Optional<IntersectionInfo> intersect(Ray ray);
}
