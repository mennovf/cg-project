package light;

import film.RGBSpectrum;
import math.Point;
import math.Ray;

import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 15/02/2017.
 */
public interface Light {
    class LightSample {
        public Point point;
        public RGBSpectrum power;
        public double g;
        public double p;

        public LightSample(RGBSpectrum power, Point point, double g, double p) {
            this.power = power;
            this.point = point;
            this.g = g;
            this.p = p;
        }
    }

    /**
     * Samples the light sources using the uniform random variable chance as its source of randomness.
     * @param from
     *          The point to light.  @return The spectrum emitted by the light source at point p in direction omega.
     */
    LightSample sample(Random random, Point from);
    Optional<LightSample> intersect(Ray ray);
}
