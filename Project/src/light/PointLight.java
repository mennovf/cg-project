package light;

import film.RGBSpectrum;
import math.Point;
import math.Ray;

import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 15/02/2017.
 */
public class PointLight implements Light {
    RGBSpectrum normalized_power;
    Point position;

    public PointLight(RGBSpectrum power, Point position) {
        this.normalized_power = power.divide(4*Math.PI);
        this.position = position;
    }

    @Override
    public LightSample sample(Random random, Point from) {
        return new LightSample(normalized_power, position, 1. / position.subtract(from).lengthSquared(), 1.);
    }

    @Override
    public Optional<LightSample> intersect(Ray ray) {
        return Optional.empty();
    }
}
