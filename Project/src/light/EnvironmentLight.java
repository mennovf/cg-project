package light;

import film.RGBSpectrum;
import material.texture.Bitmap;
import material.texture.Texture;
import math.Point;
import math.Ray;
import math.Vector;

import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 16/05/2017.
 */
public class EnvironmentLight implements Light {
    final static double INFTY = 1e8;

    public Texture map;
    public double amplification;

    public EnvironmentLight(Texture m, double a) {
        this.map = m;
        this.amplification = a;
    }

    @Override
    public LightSample sample(Random random, Point from) {
        final double u1 = random.nextDouble(), u2 = random.nextDouble();
        final double side = random.nextDouble() < .5 ? -1. : 1.;
        final double theta = Math.acos(u1);
        final double r = Math.sqrt(1. - u1*u1);
        final double phi = 2*Math.PI * u2;

        final double u = theta / Math.PI;
        final double v = 0.5 * phi / Math.PI;

        Point point = from.add((new Vector(Math.cos(phi - Math.PI)*r, Math.sin(phi - Math.PI)*r, side*u1)).scale(INFTY));


        return new LightSample(map.at(u, v).scale(this.amplification), point, 1.0, 1. / (2.*Math.PI));
    }

    @Override
    public Optional<LightSample> intersect(Ray ray) {
        final double theta = Math.acos(ray.direction.z);
        final double phi = Math.atan2(ray.direction.y, ray.direction.x) + Math.PI;

        final double u = theta / Math.PI;
        final double v = 0.5 * phi / Math.PI;

        return Optional.of(new LightSample(map.at(u, v).scale(this.amplification), ray.at(INFTY), -1.0, -1.0));
    }
}
