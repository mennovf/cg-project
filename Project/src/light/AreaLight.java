package light;

import film.RGBSpectrum;
import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;
import shape.Shape;
import shape.TriangleMesh;

import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 15/03/2017.
 */
public class AreaLight implements Light {
    public Transformation T;
    public TriangleMesh shape;
    public RGBSpectrum Le;
    public Directional directional;
    private double area;
    private double inv_area;

    public enum Directional {
        UNI,
        BI
    }

    public AreaLight(RGBSpectrum power, TriangleMesh shape, Transformation t, Directional directional) {
        this.shape = shape;
        this.T = t;
        this.area = shape.area(t);
        this.Le = power.divide(Math.PI * this.area);
        this.inv_area = 1. / this.area;
        this.directional = directional;
    }

    @Override
    public LightSample sample(Random random, Point from) {
        // Select a random triangle, proportional to it's surface area
        TriangleMesh.Tri random_tri = null;
        double random_cumul_area = random.nextDouble() * this.area;
        double cumul_area = 0;
        for (TriangleMesh.Tri tri : this.shape.tris) {
            cumul_area += tri.area(this.T);
            if (cumul_area >= random_cumul_area) {
                random_tri = tri;
                break;
            }
        }
        assert(random_tri != null);

        // Select a random point on the triangle
        double s = random.nextDouble();
        double t = random.nextDouble();

        if (s+t > 1) {
            s = 1 - s;
            t = 1 - t;
        }

        Point p = this.T.transform(random_tri.at(s, t));
        Vector n = this.T.transformAsNormal(random_tri.normalAt(s, t));
        double cos = Vector.dot(n, from.subtract(p).normalized());
        boolean hit = (this.directional == Directional.BI) || (cos > 0.);
        return new LightSample(hit ? this.Le : RGBSpectrum.BLACK, p, cos / p.subtract(from).lengthSquared(), this.inv_area);
    }

    @Override
    public Optional<LightSample> intersect(Ray ray) {
        Optional<Shape.Intersection> oisect = shape.intersect(this.T.transformInverse(ray));
        if (!oisect.isPresent()) return Optional.empty();

        Shape.Intersection isect = oisect.get();
        // LightSample.p = -1.0 because it shouldn't be used in this scenario
        Point point = this.T.transform(isect.intersection);
        Vector n = this.T.transformAsNormal(isect.normal);
        // Cos factor not included in pathtracing formulation
        double cos = Vector.dot(n, ray.origin.subtract(point).normalized());
        boolean hit = (this.directional == Directional.BI) || (cos > 0.);
        LightSample ret = new LightSample(hit ? this.Le : RGBSpectrum.BLACK, point, -1.0, -1.0);
        return Optional.of(ret);
    }
}
