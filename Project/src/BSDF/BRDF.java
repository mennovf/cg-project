package BSDF;

import math.Vector;
import java.util.Random;

/**
 * Created by Menno on 30/03/2017.
 */
public interface BRDF {
    class BRDFSample {
        public double p; // Chance of the sample
        public double f; // Value of the brdf evaluated in this sample
        public Vector direction; // Generated direction on the hemisphere

        public BRDFSample(double p, double f, Vector out) {
            this.p = p;
            this.f = f;
            this.direction = out;
        }
    }

    BRDF.BRDFSample sample(Random r, Vector in);
    double f(Vector in, Vector out);
}
