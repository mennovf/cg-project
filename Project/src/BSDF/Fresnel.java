package BSDF;

import math.Vector;

import java.util.Random;

/**
 * Created by Menno on 15/05/2017.
 */
public class Fresnel implements BRDF {

    double ior;
    static Mirror mirror = new Mirror();

    public Fresnel(double ior) {
        this.ior = ior;
    }

    @Override
    public BRDFSample sample(Random r, Vector in) {
        double in_ior, out_ior, dir;

        if (in.z < 0) {
            in_ior = 1.;
            out_ior = this.ior;
            dir = -1.;
        } else {
            in_ior = this.ior;
            out_ior = 1.;
            dir = 1.;
        }

        double cos_theta_i = Math.abs(in.z);
        double sin_theta_i = Math.sqrt(1 - cos_theta_i*cos_theta_i);
        double sin_theta_t = sin_theta_i * in_ior / out_ior;

        // Total Internal Reflection
        if (sin_theta_t > 1.) {
            return mirror.sample(null, in);
        }

        double cos_theta_t = Math.sqrt(1 - sin_theta_t*sin_theta_t);

        double r_par = (out_ior*cos_theta_i - in_ior*cos_theta_t) / (out_ior*cos_theta_i + in_ior*cos_theta_t);
        double r_orth = (in_ior*cos_theta_i - out_ior*cos_theta_t) / (in_ior*cos_theta_i + out_ior*cos_theta_t);

        double Fr = 0.5*(r_par*r_par + r_orth*r_orth);

        // Determine transmission or reflection
        if (r.nextDouble() < Fr) {
            // Reflection
            return mirror.sample(null, in);
        } else {
            // Transmission
            double scaling = sin_theta_t / Math.sqrt(in.x*in.x + in.y*in.y);
            return new BRDFSample(1., (out_ior*out_ior)/(in_ior*in_ior*cos_theta_i), new Vector(scaling*in.x, scaling*in.y, dir*cos_theta_t));
        }
    }

    @Override
    public double f(Vector in, Vector out) {
        return 0.;
    }
}
