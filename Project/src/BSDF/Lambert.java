package BSDF;

import math.Vector;

import java.util.Random;

/**
 * Created by Menno on 30/03/2017.
 */
public class Lambert implements BRDF {
    @Override
    public BRDF.BRDFSample sample(Random random, Vector in) {
        double u1 = random.nextDouble();
        double u2 = random.nextDouble();
        double r = Math.sqrt(u1);
        double theta = 2.*Math.PI*u2;

        double x = r*Math.cos(theta);
        double y = r*Math.sin(theta);
        double z = Math.sqrt(Math.max(0, 1 - u1));

        return new BRDFSample(z/Math.PI, 1./Math.PI, new Vector(x, y, z));
    }

    @Override
    public double f(Vector in, Vector out) {
        return 1. / Math.PI;
    }
}
