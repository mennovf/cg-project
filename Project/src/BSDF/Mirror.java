package BSDF;

import math.Vector;

import java.util.Random;

/**
 * Created by Menno on 31/03/2017.
 */
public class Mirror implements BRDF {
    @Override
    public BRDFSample sample(Random r, Vector in) {
        return new BRDFSample(1.0, 1. / Math.abs(in.z), new Vector(in.x, in.y, -in.z));
    }

    @Override
    public double f(Vector in, Vector out) {
        return 0.;
    }
}
