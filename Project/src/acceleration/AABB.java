package acceleration;

import math.Point;
import math.Ray;
import math.Util;
import math.Vector;
import shape.TriangleMesh;

import java.nio.file.FileSystems;
import java.util.Collection;
import java.util.Optional;
import java.util.Random;

/**
 * Created by Menno on 1/03/2017.
 */
public class AABB {
    public static final double EPS = 1-7;
    public Point pMin, pMax;

    public AABB() {
        pMin = new Point(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        pMax = new Point(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
    }

    public AABB(Point p) {
        pMin = p;
        pMax = p;
    }

    public AABB(Collection<Point> points) {
        this();
        for (Point p: points) {
            this.expand(p);
        }
        this.fix_degeneracy();
    }

    public void fix_degeneracy() {
        if (Util.dist(pMin.x, pMax.x) < EPS) {
            pMin = new Point(pMin.x - EPS, pMin.y, pMin.z);
            pMax = new Point(pMax.x + EPS, pMax.y, pMax.z);
        }
        if (Util.dist(pMin.y, pMax.y) < EPS) {
            pMin = new Point(pMin.x, pMin.y - EPS, pMin.z);
            pMax = new Point(pMax.x, pMax.y + EPS, pMax.z);
        }
        if (Util.dist(pMin.z, pMax.z) < EPS) {
            pMin = new Point(pMin.x, pMin.y, pMin.z - EPS);
            pMax = new Point(pMax.x, pMax.y, pMax.z + EPS);
        }
    }

    public boolean is_inside(Point p) {
        return (p.x >= pMin.x && p.x <= pMax.x &&
                p.y >= pMin.y && p.y <= pMax.y &&
                p.z >= pMin.z && p.z <= pMax.z);
    }

    public void expand(Point p) {
        pMin = new Point(Math.min(pMin.x, p.x), Math.min(pMin.y, p.y), Math.min(pMin.z, p.z));
        pMax = new Point(Math.max(pMax.x, p.x), Math.max(pMax.y, p.y), Math.max(pMax.z, p.z));
    }

    public void expand(AABB aabb) {
        this.expand(aabb.pMin);
        this.expand(aabb.pMax);
    }

    public void translate(Vector offset) {
        this.pMin = this.pMin.add(offset);
        this.pMax = this.pMax.add(offset);
    }

    public static class AABBIntersection {
        double tmin = Double.NEGATIVE_INFINITY;
        double tmax = Double.POSITIVE_INFINITY;
    }

    /**
     * Take from here http://psgraphics.blogspot.be/2016/02/new-simple-ray-box-test-from-andrew.html
     */
    public Optional<AABBIntersection> intersect(final Ray r) {
        r.increment_tests(1);
        final double[] o = {r.origin.x, r.origin.y, r.origin.z};
        final double[] d = {r.direction.x, r.direction.y, r.direction.z};
        final double[] min = {pMin.x, pMin.y, pMin.z};
        final double[] max = {pMax.x, pMax.y, pMax.z};
        AABBIntersection ret = new AABBIntersection();
        for (int i = 0; i<3; ++i) {
            double invD = 1.0 / d[i];
            double t0 = (min[i] - o[i]) * invD;
            double t1 = (max[i] - o[i]) * invD;
            if (invD < 0.0) {
                final double temp = t1;
                t1 = t0;
                t0 = temp;
            }
            ret.tmin = (t0 > ret.tmin) ? t0 : ret.tmin;
            ret.tmax = (t1 < ret.tmax) ? t1 : ret.tmax;

            if (ret.tmax < ret.tmin) {
                return Optional.empty();
            }
        }
        if (ret.tmax <= 0) {
            return Optional.empty();
        }
        return Optional.of(ret);
    }

    public static void main(String[] args) {
        // Proposed aabb test
        TriangleMesh mesh = new TriangleMesh(FileSystems.getDefault().getPath("models", "plane.obj"), TriangleMesh.Type.SEQUENTIAL);
        AABB aabb = mesh.AABB();
        Random r = new Random();
        int k = 0, l = 0;
        for (int i = 0; i < 1e7; ++i) {
            double otheta = r.nextDouble();
            double ophi = r.nextDouble();
            double dtheta = r.nextDouble();
            double dphi = r.nextDouble();
            double R = 1;
            Point o = new Point(R*Math.sin(otheta)*Math.cos(ophi), R*Math.sin(otheta)*Math.sin(ophi), R*Math.cos(otheta));
            Ray ray = new Ray(o, (new Point(-1.5+3*dtheta, 0, -1.5+3*dphi)).subtract(o).normalized());

            Optional<AABB.AABBIntersection> aisect = aabb.intersect(ray);
            Optional<TriangleMesh.Intersection> misect = mesh.intersect(ray);

            if (!misect.isPresent()) {
                k += 1;
            }

            if (misect.isPresent() && !aisect.isPresent()) {
                System.out.println("This is wrong");
            }
            if (!misect.isPresent() && aisect.isPresent()) {
                l += 1;
            }
        }
        System.out.println(k);
        System.out.println(l);
    }
}
