package acceleration;

import math.Ray;

import java.util.Optional;

/**
 * Created by Menno on 2/03/2017.
 */
public interface Boundable<Intersection> {
    AABB AABB();
    Optional<Intersection> intersect(Ray ray);
}
