package acceleration;

import math.Point;
import math.Ray;
import math.Vector;
import shape.TriangleMesh;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Menno on 2/03/2017.
 * Idee: BVHNode is ofwel een (AABB + 2*BVHNode) of een (AABB + IBoundable)
 * Generic klasse in IBoundable war IBoundable een .getAABB methode heeft.
 */
public class BVH<T extends Boundable, CustomIntersection extends BVH.BasicIntersection> {

    public BVHNode root;
    public interface BasicIntersection {
        double t(); // Ray parameter
    }

    public abstract class BVHNode {
        public AABB aabb;
        public abstract Optional<CustomIntersection> intersect(Ray ray);
        public abstract boolean contains(T t);
    }

    /**
     * Helper class used for the construction of the BVH.
     */
    public class ConstructionNode {
        public ArrayList<Leaf> children;

        /**
         * Recursively subdivides the array of leaves into hierarchy of twonodes untill
         * the leafs contain n elements.
         * @param n
         * @return
         */
        public BVHNode subdivide(final int n) {
            // Recursive base case
            if (this.children.size() <= n) {
                return new Leaf(this.children);
            }

            TwoNode ret = new TwoNode();
            ConstructionNode pre_left = new ConstructionNode();
            ConstructionNode pre_right = new ConstructionNode();

            // Calculate aabb
            ret.aabb = new AABB();
            for (Leaf child : children) {
                ret.aabb.expand(child.aabb);
            }

            // Find dimension with largest extent
            final double[] extents = {ret.aabb.pMax.x - ret.aabb.pMin.x,ret.aabb.pMax.y - ret.aabb.pMin.y,ret.aabb.pMax.z - ret.aabb.pMin.z};
            int largest_extent;
            if (extents[0] > extents[1]) largest_extent = 0;
            else if (extents[1] > extents[2]) largest_extent = 1;
            else largest_extent = 2;
            // Sort the list according to this extent
            children.sort((left, right)->{
                final double lcenter, rcenter;
                // Division by two is not necessary
                if (largest_extent == 0) {lcenter = left.aabb.pMin.x + left.aabb.pMax.x; rcenter = right.aabb.pMin.x + right.aabb.pMax.x;}
                else if (largest_extent == 1) {lcenter = left.aabb.pMin.y + left.aabb.pMax.y; rcenter = right.aabb.pMin.y + right.aabb.pMax.y;}
                else {lcenter = left.aabb.pMin.z + left.aabb.pMax.z; rcenter = right.aabb.pMin.z + right.aabb.pMax.z;}
                return Double.compare(lcenter, rcenter);
            });

            // Assign leafs to subnode
            int mid = children.size() / 2;
            pre_left.children = new ArrayList<>(children.subList(0, mid));
            pre_right.children = new ArrayList<>(children.subList(mid, children.size()));

            // Fill in TwoNode left and right by recursively subdividing
            ret.left = pre_left.subdivide(n);
            ret.right = pre_right.subdivide(n);
            return ret;
        }
    }

    public class TwoNode extends BVHNode {
        public BVHNode left, right;

        @Override
        public Optional<CustomIntersection> intersect(Ray ray) {
            Optional<AABB.AABBIntersection> lisect_aabb = left.aabb.intersect(ray);
            Optional<AABB.AABBIntersection> risect_aabb = right.aabb.intersect(ray);

            if (!lisect_aabb.isPresent() && !risect_aabb.isPresent()) {
                return Optional.empty();
            }
            BVHNode closest;
            BVHNode farthest;
            double farthest_mint = Double.POSITIVE_INFINITY;

            if (lisect_aabb.isPresent()) {
                if (risect_aabb.isPresent()) {
                    // Both are present
                    closest = (lisect_aabb.get().tmin < risect_aabb.get().tmin) ? left : right;
                    farthest = (lisect_aabb.get().tmin < risect_aabb.get().tmin) ? right : left;
                    farthest_mint = (lisect_aabb.get().tmin < risect_aabb.get().tmin) ? risect_aabb.get().tmin : lisect_aabb.get().tmin;
                } else {
                    // Only the left one is present
                    closest = left;
                    farthest = null;
                }
            } else { // Both being not present has already been checked above.
                // Only the right one is present
                closest = right;
                farthest = null;
            }

            Optional<CustomIntersection> closest_isect = closest.intersect(ray);
            if (closest_isect.isPresent()) {
                // Check if it's not contained within the farthest bounding box
                if (farthest != null && closest_isect.get().t() >= farthest_mint) {
                    Optional<CustomIntersection> farthest_isect = farthest.intersect(ray);
                    if (farthest_isect.isPresent() && farthest_isect.get().t() < closest_isect.get().t()) {
                        return farthest_isect;
                    }
                }
                return closest_isect;
            } else if (farthest != null) {
                return farthest.intersect(ray);
            } else {
                return Optional.empty();
            }
        }

        @Override
        public boolean contains(T t) {
            return left.contains(t) || right.contains(t);
        }
    }

    public class Leaf extends BVHNode {
        ArrayList<T> elements;

        public Leaf(T element) {
            this.elements = new ArrayList<>();
            this.elements.add(element);
            this.aabb = element.AABB();
        }

        public Leaf(Collection<Leaf> leafs) {
            this.elements = new ArrayList<>();
            this.aabb = new AABB();

            for (Leaf leaf: leafs) {
                this.elements.addAll(leaf.elements);
            }
            for (T element: this.elements) {
                this.aabb.expand(element.AABB());
            }
        }

        @Override
        public Optional<CustomIntersection> intersect(Ray ray) {
            CustomIntersection best_isect = null;
            for (T element: this.elements) {
                Optional<CustomIntersection> cisect = element.intersect(ray);
                if (cisect.isPresent()) {
                    if (best_isect == null) {best_isect = cisect.get(); continue;}
                    else if (cisect.get().t() < best_isect.t()) {best_isect = cisect.get();}
                }
            }
            return Optional.ofNullable(best_isect);
        }

        @Override
        public boolean contains(T t) { return this.elements.contains(t); }

    }

    public Optional<CustomIntersection> intersect (Ray ray) {
        Optional<AABB.AABBIntersection> aabb_isect = root.aabb.intersect(ray);
        if (aabb_isect.isPresent()) {
            return root.intersect(ray);
        }
        return Optional.empty();
    }

    public BVH(Collection<T> elements, final int n) {
        ConstructionNode pre_root = new ConstructionNode();
        pre_root.children = new ArrayList<>(elements.size());

        pre_root.children.addAll(elements.stream().map(Leaf::new).collect(Collectors.toList()));

        this.root = pre_root.subdivide(n);
    }

    public static void main(String[] args) {
        Path p = FileSystems.getDefault().getPath("models", "cube.obj");
        TriangleMesh mesh = new TriangleMesh(p, TriangleMesh.Type.SEQUENTIAL);
        TriangleMesh bvhmesh = new TriangleMesh(p, TriangleMesh.Type.BVH);

        Random r = new Random();
        int k = 0;
        for (int i = 0; i < 1e7; ++i) {
            double otheta = r.nextDouble();
            double ophi = r.nextDouble();
            double dtheta = r.nextDouble();
            double dphi = r.nextDouble();
            double R = 1;
            Ray ray = new Ray(new Point(R*Math.sin(otheta)*Math.cos(ophi), R*Math.sin(otheta)*Math.sin(ophi), R*Math.cos(otheta)),
                    new Vector(Math.sin(dtheta)*Math.cos(dphi), Math.sin(dtheta)*Math.sin(dphi), Math.cos(dtheta)));

            Optional<TriangleMesh.Intersection> aisect = bvhmesh.intersect(ray);
            Optional<TriangleMesh.Intersection> misect = mesh.intersect(ray);

            if (misect.isPresent() && !aisect.isPresent()) {
                Optional<TriangleMesh.Intersection> is = mesh.intersect(ray);
                bvhmesh.intersect(ray);
                k += 1;
            }
        }

        System.out.println(k);
}

}
