package shape;

import acceleration.AABB;
import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Menno on 14/02/2017.
 */
public class Cylinder implements Shape {
    @Override
    public AABB AABB() {
        AABB ret = new AABB();
        ret.expand(new Point(-1, 0, -1));
        ret.expand(new Point(1, 1, 1));
        return ret;
    }

    @Override
    public Optional<Intersection> intersect(Ray ray) {
        ray.increment_tests(1);
        // Y is the up vector
        final Point o = ray.origin;
        final Vector dir = ray.direction;

        List<Intersection> intersections = new ArrayList<>();

        // Check the mantle of the cylinder for intersections
        double a = dir.x * dir.x + dir.z*dir.z;
        double b = 2.0 * (dir.x*o.x + dir.z*o.z);
        double c = (o.x*o.x + o.z*o.z) - 1.0;

        double d = b * b - 4.0 * a * c;

        if (d >= 0) {
            double dr = Math.sqrt(d);

            // numerically solve the equation a*t^2 + b * t + c = 0
            double q = -0.5 * (b < 0 ? (b - dr) : (b + dr));

            double t0 = q / a;
            double t1 = c / q;

            if (t0 >= 0) {
                double y_pos = ray.at(t0).y;
                if (y_pos <= 1 && y_pos >= 0) {
                    Intersection isect = new Intersection();
                    final Point p = ray.at(t0);
                    isect.intersection = p;
                    isect.normal = p.subtract(0, 0, 0);
                    isect.t = t0;
                    intersections.add(isect);
                }
            }
            if (t1 >= 0) {
                double y_pos = ray.at(t1).y;
                if (y_pos <= 1 && y_pos >= 0) {
                    Intersection isect = new Intersection();
                    final Point p = ray.at(t1);
                    isect.intersection = p;
                    isect.normal = p.subtract(0, 0, 0);
                    isect.t = t1;
                    intersections.add(isect);
                }
            }
        }

        // Check the upper and lower bounds of the cylinder
        double t2 = (0 - o.y) / dir.y;
        if (t2 >= 0) {
            final Point ipoint = ray.at(t2);
            if (ipoint.x*ipoint.x + ipoint.z*ipoint.z <= 1) {
                Intersection isect = new Intersection();
                isect.intersection = ipoint;
                isect.normal = new Vector(0, -1, 0);
                isect.t = t2;
                intersections.add(isect);
            }
        }
        double t3 = (1 - o.y) / dir.y;
        if (t3 >= 0) {
            final Point ipoint = ray.at(t3);
            if (ipoint.x*ipoint.x + ipoint.z*ipoint.z <= 1) {
                Intersection isect = new Intersection();
                isect.intersection = ipoint;
                isect.normal = new Vector(0, 1, 0);
                isect.t = t3;
                intersections.add(isect);
            }
        }


        if (intersections.isEmpty()) return Optional.empty();
        return Optional.of(Collections.min(intersections, (left, right) -> Double.compare(left.t, right.t)));
    }
}
