package shape;

import acceleration.AABB;
import acceleration.Boundable;
import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by Menno on 15/02/2017.
 * A class for a trianglemesh
 */
public class TriangleMesh implements Shape {

    final static double EPS = 1e-7;
    public class Tri implements Boundable<Intersection> {
        public int v[] = new int[3];
        public int n[] = new int[3];
        public int t[] = new int[3];

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Tri tri = (Tri) o;

            if (!Arrays.equals(v, tri.v)) return false;
            if (!Arrays.equals(n, tri.n)) return false;
            return Arrays.equals(t, tri.t);

        }

        @Override
        public int hashCode() {
            int result = Arrays.hashCode(v);
            result = 31 * result + Arrays.hashCode(n);
            result = 31 * result + Arrays.hashCode(t);
            return result;
        }

        @Override
        public AABB AABB() {
            return new AABB(Arrays.asList(TriangleMesh.this.vertices.get(v[0]),
                                          TriangleMesh.this.vertices.get(v[1]),
                                          TriangleMesh.this.vertices.get(v[2])));
        }

        @Override
        public String toString() {
            return  TriangleMesh.this.vertices.get(v[0]).toString() + "\n" +
                    TriangleMesh.this.vertices.get(v[1]).toString() + "\n" +
                    TriangleMesh.this.vertices.get(v[2]).toString();
        }

        @Override
        public Optional<Intersection> intersect(Ray ray) {
            ray.increment_tests(1);
            final Point o = ray.origin;
            final Vector d = ray.direction.normalized();

            // Muller-Trumbore triangle intersection
            final Point v0 = vertices.get(this.v[0]);
            final Point v1 = vertices.get(this.v[1]);
            final Point v2 = vertices.get(this.v[2]);

            Vector e1, e2;  //Edge1, Edge2
            Vector P, Q, T;
            double det, inv_det, u, v;
            double t;

            e1 = v1.subtract(v0);
            e2 = v2.subtract(v0);

            P = Vector.cross(d, e2);
            det = Vector.dot(e1, P);

            if (det > - EPS && det < EPS) return Optional.empty();
            inv_det = 1.0 / det;

            T = o.subtract(v0);
            u = Vector.dot(T, P) * inv_det;

            if (u < 0 || u > 1) return Optional.empty();

            Q = Vector.cross(T, e1);
            v = Vector.dot(d, Q) * inv_det;

            if (v < 0 || u+v > 1) return Optional.empty();

            t = Vector.dot(e2, Q) * inv_det;

            if (t > EPS) {
                // Hit
                Intersection isect = new Intersection();
                isect.t = t;
                isect.intersection = this.at(u, v);
                // Phong interpolation of normals
                isect.normal = this.normalAt(u, v);
                if (this.t[0] != -1) {
                    final TextureCoordinate txcoord = texture_coordinates.get(this.t[0]).scale(1-u-v).add(
                            texture_coordinates.get(this.t[1]).scale(u)).add(
                            texture_coordinates.get(this.t[2]).scale(v));
                    isect.u = txcoord.u;
                    isect.v = txcoord.v;
                }

                return Optional.of(isect);
            }

            return Optional.empty();
        }

        public double area(Transformation t) {
            Vector e1 = TriangleMesh.this.vertices.get(this.v[1]).subtract(
                        TriangleMesh.this.vertices.get(this.v[0]));
            Vector e2 = TriangleMesh.this.vertices.get(this.v[2]).subtract(
                        TriangleMesh.this.vertices.get(this.v[0]));
            return Vector.cross(t.transform(e1), t.transform(e2)).length() / 2;
        }

        public Point at(double u, double v) {
            return TriangleMesh.this.vertices.get(this.v[0]).add(
                    (TriangleMesh.this.vertices.get(this.v[1]).subtract(TriangleMesh.this.vertices.get(this.v[0]))).scale(u)).add(
                    (TriangleMesh.this.vertices.get(this.v[2]).subtract(TriangleMesh.this.vertices.get(this.v[0]))).scale(v));
        }

        public Vector normalAt(double u, double v) {
            if (this.n[0] == -1) {
                Vector e1 = TriangleMesh.this.vertices.get(this.v[1]).subtract(TriangleMesh.this.vertices.get(this.v[0]));
                Vector e2 = TriangleMesh.this.vertices.get(this.v[2]).subtract(TriangleMesh.this.vertices.get(this.v[0]));
                return Vector.cross(e1, e2).normalized();
            } else {
                return Vector.add(Vector.add(TriangleMesh.this.normals.get(this.n[0]).scale(1-u-v),
                        TriangleMesh.this.normals.get(this.n[1]).scale(u))
                        , TriangleMesh.this.normals.get(this.n[2]).scale(v));
            }
        }
    }

    public class TextureCoordinate {
        double u, v, w;

        public TextureCoordinate(double u, double v, double w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }

        public TextureCoordinate(double u, double v) {
            this(u, v, 0);
        }

        public TextureCoordinate add(TextureCoordinate right) {
            return new TextureCoordinate(this.u+right.u, this.v+right.v, this.w+right.w);
        }
        public TextureCoordinate scale(double s) {
            return new TextureCoordinate(this.u*s, this.v*s, this.w*s);
        }
    }

    public interface TriStorage extends Boundable<Intersection> {
        @Override
        Optional<Intersection> intersect(Ray ray);
    }

    public class Sequential implements TriStorage {
        List<Tri> tris;

        public Sequential(Collection<Tri> tris) {
            this.tris = new ArrayList<>(tris);
        }

        @Override
        public AABB AABB() {
            AABB ret = new AABB();
            for (Tri tri : tris) {
                ret.expand(tri.AABB());
            }
            return ret;
        }

        @Override
        public Optional<Intersection> intersect(Ray ray) {
            Intersection isect = null;
            for (Tri tri : tris) {
                Optional<Intersection> triisect = tri.intersect(ray);

                if (triisect.isPresent()) {
                    if (isect == null) { isect = new Intersection(); isect.t = Double.POSITIVE_INFINITY; }
                    if (isect.t > triisect.get().t) {
                        isect = triisect.get();
                    }
                }
            }

            return Optional.ofNullable(isect);
        }
    }

    public class BVH implements TriStorage {
        public acceleration.BVH<Tri, Shape.Intersection> tris;

        public BVH(Collection<Tri> tris) {
            this.tris = new acceleration.BVH<>(tris, 10);
        }

        @Override
        public AABB AABB() {
            return tris.root.aabb;
        }

        @Override
        public Optional<Intersection> intersect(Ray ray) {
            return tris.intersect(ray);
        }
    }

    public enum Type {
        SEQUENTIAL,
        BVH
    }

    private int index(int in) {
        return (in > 0) ? (in - 1) : (this.vertices.size() + in);
    }

    public TriangleMesh(Path path, Type type) {
        boolean hasNormals = false;

        try {
            Files.lines(path).forEach((line)->{
                if (line.startsWith("#") || line.isEmpty()) return; // Comment or empty
                String[] parts = line.split("\\s+");

                String command = parts[0];

                if (command.equals("v")) {
                    vertices.add(new Point(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), Double.parseDouble(parts[3])));
                }
                if (command.equals("vt")) {
                    double u = Double.parseDouble(parts[1]);
                    double v = Double.parseDouble(parts[2]);
                    switch (parts.length) {
                        case 3: texture_coordinates.add(new TextureCoordinate(u, v)); break;
                        case 4: texture_coordinates.add(new TextureCoordinate(u, v, Double.parseDouble(parts[3]))); break;
                    }
                }
                if (command.equals("vn")) {
                    normals.add(new Vector(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), Double.parseDouble(parts[3])));
                }
                if (command.equals("f")) {
                    // TODO: Implement completer version of obj
                    if (parts.length == 4) {
                        Tri tri = new Tri();
                        for (int i = 0; i < 3; ++i) {
                            parse_face_vert(tri, parts[i+1], i);
                        }
                        tris.add(tri);
                    } else if (parts.length == 5) {
                        Tri tri1 = new Tri();
                        Tri tri2 = new Tri();
                        int idxs[] = new int[]{0, 2, 3};
                        for (int i = 0; i < 3; ++i) {
                            parse_face_vert(tri1, parts[i+1], i);
                            parse_face_vert(tri2, parts[idxs[i]+1], i);
                        }
                        tris.add(tri1);
                        tris.add(tri2);
                    }
                }
            });
        } catch (IOException e) {
            System.err.println("Unable to open file: "+path);
        }

        switch (type) {
            case SEQUENTIAL: storage = new Sequential(tris); break;
            case BVH: storage = new BVH(tris); break;
        }
    }

    protected void parse_face_vert(Tri tri, String s, int i) {
        String[] vert = s.split("/");
        switch (vert.length) {
            case 3: {
                tri.v[i] = this.index(Integer.parseInt(vert[0]));
                tri.t[i] = this.index(Integer.parseInt(vert[1]));
                tri.n[i] = this.index(Integer.parseInt(vert[2]));
                break;
            }
            case 1: {
                tri.v[i] = this.index(Integer.parseInt(vert[0]));
                tri.t[i] = -1;
                tri.n[i] = -1;
                break;
            }
        }
    }

    public List<Point> vertices = new ArrayList<>();
    public List<TextureCoordinate> texture_coordinates = new ArrayList<>();
    public List<Vector> normals = new ArrayList<>();
    public List<Tri> tris = new ArrayList<>();
    public TriStorage storage;

    static public void main(String[] args) {
        // Test the amount of tris in the leaf
        Path p = FileSystems.getDefault().getPath("models", "teapot.obj");

        Random r = new Random();
        for (int k = 1; k <= 20; ++k) {
            TriangleMesh bvhmesh = new TriangleMesh(p, TriangleMesh.Type.BVH/*,k*/);
            long duration = 0;
            for (int i = 0; i < 1e6; ++i) {
                double otheta = r.nextDouble();
                double ophi = r.nextDouble();
                double dtheta = r.nextDouble();
                double dphi = r.nextDouble();
                double R = 3;
                Point o = new Point(R*Math.sin(otheta)*Math.cos(ophi), R*Math.sin(otheta)*Math.sin(ophi), R*Math.cos(otheta));
                Ray ray = new Ray(o, (new Point(-1.5+3*dtheta, 0, -1.5+3*dphi)).subtract(o).normalized());

                long start_time = System.nanoTime();
                Optional<TriangleMesh.Intersection> aisect = bvhmesh.intersect(ray);
                long end_time = System.nanoTime();
                duration += end_time - start_time;
            }
            System.out.println(Integer.toString(k) + ": " + Double.toString(duration/1.0e9) + "s");
        }
    }

    @Override
    public Optional<Intersection> intersect(Ray ray) {
        return storage.intersect(ray);
    }

    @Override
    public AABB AABB() {
        return storage.AABB();
    }

    public double area(Transformation t) {
        double acc = 0;
        for (Tri tri: this.tris) {
            acc += tri.area(t);
        }
        return acc;
    }
}
