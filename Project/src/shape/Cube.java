package shape;

import acceleration.AABB;
import math.Point;
import math.Ray;
import math.Vector;

import java.util.Optional;

/**
 * Created by Menno on 15/02/2017.
 * A cube centered at 0.0 with its center at the origin.
 */
public class Cube implements Shape{
    @Override
    public AABB AABB() {
        AABB ret = new AABB();
        ret.expand(new Point(-1, -1, -1));
        ret.expand(new Point(1, 1, 1));
        return ret;
    }

    @Override
    public Optional<Intersection> intersect(Ray ray) {
        ray.increment_tests(1);
        final Point o = ray.origin;
        final Vector d = ray.direction;

        double tmin = (-1 - o.x) / d.x;
        double tmax = (1 - o.x) / d.x;
        double swap;

        if (tmin > tmax) {
            swap = tmax;
            tmax = tmin;
            tmin = swap;
        }

        double tymin = (-1 - o.y) / d.y;
        double tymax = (1 - o.y) / d.y;

        if (tymin > tymax) {
            swap = tymax;
            tymax = tymin;
            tymin = swap;
        }

        if ((tmin > tymax) || (tymin > tmax))
            return Optional.empty();

        if (tymin > tmin)
            tmin = tymin;

        if (tymax < tmax)
            tmax = tymax;

        double tzmin = (-1 - o.z) / d.z;
        double tzmax = (1 - o.z) / d.z;

        if (tzmin > tzmax) {
            swap = tzmax;
            tzmax = tzmin;
            tzmin = swap;
        }

        if ((tmin > tzmax) || (tzmin > tmax))
            return Optional.empty();

        if (tzmin > tmin)
            tmin = tzmin;

        if (tzmax < tmax)
            tmax = tzmax;

        double t = tmin;
        if (tmin <= 0) {
            if (tmax <=0) return Optional.empty();
            t = tmax;
        }

        Intersection isect = new Intersection();
        final Point p = ray.at(t);
        isect.intersection = p;
        isect.t = t;

        double xm = Math.abs(p.x);
        double ym = Math.abs(p.y);
        double zm = Math.abs(p.z);

        if (xm >= ym && xm >= zm) isect.normal = new Vector(Math.signum(p.x), 0, 0);
        else if (ym >= zm) isect.normal = new Vector(0, Math.signum(p.y), 0);
        else isect.normal = new Vector(0, 0, Math.signum(p.z));

        return Optional.of(isect);
    }
}
