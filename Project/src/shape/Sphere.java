package shape;

import acceleration.AABB;
import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;

import java.util.Optional;

/**
 * Represents a three-dimensional sphere with radius one, centered at the
 * origin.
 * 
 * @author Niels Billen, Menno Vanfrachem
 * @version 0.4
 */
public class Sphere implements Shape {
    @Override
    public AABB AABB() {
        AABB ret = new AABB();
        ret.expand(new Point(-1, -1, -1));
        ret.expand(new Point(1, 1, 1));
        return ret;
    }

    /*
         * (non-Javadoc)
         *
         * @see shape.Shape#intersect(geometry3d.Ray3D)
         */
	@Override
	public Optional<Intersection> intersect(Ray ray) {
		ray.increment_tests(1);
		if (ray == null)
			return Optional.empty();

		Vector o = ray.origin.toVector();

		double a = ray.direction.lengthSquared();
		double b = 2.0 * (Vector.dot(ray.direction, o));
		double c = Vector.dot(o, o) - 1.0;

		double d = b * b - 4.0 * a * c;

		if (d < 0)
			return Optional.empty();
		double dr = Math.sqrt(d);

		// numerically solve the equation a*t^2 + b * t + c = 0
		double q = -0.5 * (b < 0 ? (b - dr) : (b + dr));

		double t0 = q / a;
		double t1 = c / q;

		if (t0 >=0 || t1>=0) {
			double t;
			if (t0 >= 0 && t1 >= 0) t = Math.min(t0, t1);
			else {t = t0 >= 0 ? t0 : t1;}
            Intersection isect = new Intersection();
            final Point p = ray.at(t);
            isect.intersection = p;
            isect.normal = p.subtract(0, 0, 0);
            isect.t = t;
            return Optional.of(isect);
		}
		return Optional.empty();
	}
}
