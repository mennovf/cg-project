package shape;

import acceleration.BVH;
import acceleration.Boundable;
import math.Point;
import math.Ray;
import math.Vector;

import java.util.Optional;

/**
 * Interface which should be implemented by all shapes.
 * 
 * @author Niels Billen, Menno Vanfrachem
 * @version 0.4
 */
public interface Shape extends Boundable<Shape.Intersection> {
	/**
	 * @param ray
	 *            the ray to intersect with.
	 * @return non-empty Optional<Intersection> if the ray has intersected.
	 */
	@Override
	Optional<Intersection> intersect(Ray ray);

	/**
	 * Class containing information of the intersection.
	 */
	class Intersection implements BVH.BasicIntersection {
		public Point intersection;
        public Vector normal;
		public double u, v;
        public double t;

        @Override
        public double t() {
            return t;
        }
    }
}
