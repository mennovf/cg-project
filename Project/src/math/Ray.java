package math;

import shape.Shape;

/**
 * Represents an ray in three dimensional space starting at a given point and
 * extending infinitely in a given direction.
 * 
 * @author Niels Billen
 * @version 0.3
 */
public class Ray implements Cloneable {
	/**
	 * The origin of the ray.
	 */
	public final Point origin;

	/**
	 * The direction the ray extends to.
	 */
	public final Vector direction;

    /**
     * Counter for the number of intersection tests this ray has been used for.
     * The type is a double so that potentially (in)expensive tests can be weighted.
     */
    public double tests;

	/**
	 * Creates a new ray starting at the given origin and propagating in the
	 * given direction.
	 * 
	 * @param origin
	 *            the origin of the ray.
	 * @param direction
	 *            the direction of the ray.
	 * @throws NullPointerException
	 *             when the given origin and/or direction is null.
	 */
	public Ray(Point origin, Vector direction) throws NullPointerException {
		if (origin == null)
			throw new NullPointerException("the given origin is null!");
		if (direction == null)
			throw new NullPointerException("the given direction is null!");
		this.origin = origin;
		this.direction = direction;
        this.tests = 0;
	}

	public Ray(Shape.Intersection isect, Vector direction, double eps) {
		this(isect.intersection.add(isect.normal.scale( Math.signum(Vector.dot(isect.normal, direction))*eps )), direction);
	}

    public void increment_tests(double amount) {
        assert (amount > 0);
        tests += amount;
    }

    /**
     * @param t
     *          The ray parameter
     * @return
     *          The point at ray parameter t
     */
	public Point at(double t) {
        return origin.add(direction.scale(t));
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("[Ray3D] from %s %s %s in direction %s %s %s",
				origin.x, origin.y, origin.z, direction.x, direction.y,
				direction.z);
	}
}
