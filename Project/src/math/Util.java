package math;

/**
 * Created by Menno on 23/02/2017.
 */
public  class Util {
    public static double clamp(double v, double a, double b) {
        return Math.min(b, Math.max(a, v));
    }

    public static double dist(double l, double r) {
        return Math.abs(l - r);
    }


    public static double lerp(double l, double r, double t) {
        return l*t + (1-t)*r;
    }

    public static int mod(int a, int x) {
        return (a % x + x) % x;
    }
}
